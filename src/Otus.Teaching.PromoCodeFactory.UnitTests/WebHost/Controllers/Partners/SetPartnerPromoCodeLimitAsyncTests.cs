﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System.Collections.Generic;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private Mock<IRepository<Partner>> _partnersRepositoryMock;
        private PartnersController _partnersController;
        private IFixture _fixture;
        private Guid _partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

        private DateTime _yesterday = DateTime.Now.AddDays(-1);
        private DateTime _today = DateTime.Now;
        private DateTime _tomorrow = DateTime.Now.AddDays(1);

        public SetPartnerPromoCodeLimitAsyncTests() 
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = _yesterday,
                        EndDate = _tomorrow,
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        [Fact]
        //Если партнер не найден, выдать ошибку 404;
        public async void SetPartnerPromoCodeLimitAsync_ParthnerNotFound_ReturnNotFound()
        {
            //Arrange
            var partnerId = Guid.Parse("70D9BA76-5B98-4830-A71E-5C0BFC672DEB".ToLower());
            Partner partner = null;

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        //Если партнер заблокирован, выдать ошибку 400;
        public async void SetPartnerPromoCodeLimitAsync_ParthnerBlocked_ReturnBadRequest()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);
            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId,request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        //Если партнеру проставляется лимит, обнулить количество промокодов, которые партнер выдал NumberIssuedPromocodes
        public async void SetPartnerPromoCodeLimitAsync_LimitSetted_ResetNumberIssuedPromocodes()
        {

            var partner = CreateBasePartner();
            partner.PartnerLimits.First().CancelDate = _tomorrow;

            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        //если лимит закончился то не обнуляется
        public async void SetPartnerPromoCodeLimitAsync_LimitSetted_NotResetNumberIssuedPromocodes()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.PartnerLimits.First().CancelDate = _yesterday;
            var issuedPromocodes = partner.NumberIssuedPromoCodes;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);
            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(issuedPromocodes);
        }

        [Fact]
        //При установке лимита отключить предыдущий лимит
        public async void SetPartnerPromoCodeLimitAsync_LimitSetted_PreviousLimitDisabled()
        {
            // Arrange
            var partner = CreateBasePartner();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest> ();

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            Assert.NotNull(activeLimit.CancelDate);
        }

        [Fact]
        //Лимит должен быть больше 0
        public async void SetPartnerPromoCodeLimitAsync_LimitShouldBePositive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            SetPartnerPromoCodeLimitRequest request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            request.Limit = -1;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        //проверить что новый лимит сохраняется в бд
        public async void SetPartnerPromoCodeLimitAsync_LimitSetted_LimitIsSavedToDataBase()
        {
            var partner = CreateBasePartner();
            partner.PartnerLimits.First().CancelDate = _tomorrow;
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest> ();
            request.Limit = 15;
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(_partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            _partnersRepositoryMock.Verify(r => r.UpdateAsync(It.Is<Partner>(p => p.Id == _partnerId && p.PartnerLimits.Any(l => l.Limit == 15))));
        }

    }
}